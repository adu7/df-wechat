import http from "../../utils/request"
async function getToken(data) {
  const code = await http.post("/wxApi/v1/auth/login", data)
  return code.data
}
export default getToken
