import http from "../../utils/request"

async function getUsetInfo() {
  const res = await http.get("/wxApi/v1/auth/userinfo")
  return res.data
}

export default getUsetInfo