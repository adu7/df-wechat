import http from "../../utils/request"
async function bindUser(data) {
  const code = await http.post("/wxApi/v1/auth/bind", data)
  return code.data
}
export default bindUser