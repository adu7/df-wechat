import http from "../../utils/request"

async function getSuperList() {
  const res = await http.get('/wxApi/v1/person/position/list')
  return res
}

export default getSuperList