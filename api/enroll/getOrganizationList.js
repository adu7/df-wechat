import http from "../../utils/request"

async function getOrganizationList() {
  const res = await http.get('/wxApi/v1/person/organization/list')
  return res
}

export default getOrganizationList