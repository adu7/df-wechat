import http from "../../utils/request"

export const useAccessDoorApi = () => {
  return http.get('/wxApi/v1/access/door/page')
}

export const useAccessOpenDoorApi = (data) => {
  return http.post('/wxApi/v1/access/command/update',data)
}