import http from "../../utils/request"

export const useAccessInoutApi = (params) => {
  return http.get('/wxApi/v1/access/inout/page',params)
}
