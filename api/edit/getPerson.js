import http from "@/utils/request";
async function getPerson(queryobj) {
  const res = await  http.get("/wxApi/v1/person/employee/query", queryobj)
  return res 
}

export default getPerson
