import http from "../../utils/request"

export const usePersonEmployeeApi = (params) => {
  return http.get('/wxApi/v1/person/employee/page',params)
}
