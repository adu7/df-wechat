const findParentCodeList = (list, rowCode) => {
  let result = [];
  function find(data, code) {
    for (let i = 0; i < data.length; i++) {
      if (data[i].code == code) {
        return {
          parentCode: data[i].parentCode,
          name: data[i].name,
        };
      }
    }
    return '';
  }

  let first = find(list, rowCode); //第一次
  let parentCode = first.parentCode
  result.push(first.name);
  if (!parentCode) {
    return result;
  }
  for (;;) {
    let obj = find(list, parentCode);
    parentCode = obj.parentCode
    result.push(obj.name);
    if (!parentCode) {
      break;
    }
  }
  result.reverse();
  return result;
};
export default findParentCodeList