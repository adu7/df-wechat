
function dataMapTrue(data, parentId) {
  const result = []
  for(const item of data) {
    if(item.parentCode === parentId) {
      const obj = {
        text: item.name,
        value: item.code,
      }
      result.push(obj)
      if(dataMapTrue(data, item.code).length) {
        obj.children = dataMapTrue(data, item.code)
      }
    }
  }
  // console.log(result)
  return result 
}

export default dataMapTrue