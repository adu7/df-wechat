const debounce = (func, delay) => {
  let timeout;
  return function() {
    const context = this; // 保存函数执行时的上下文
    const args = arguments; // 保存传入的参数

    clearTimeout(timeout); // 如果存在尚未执行的延迟函数，则清除它
    timeout = setTimeout(function() {
      func.apply(context, args); // 在wait毫秒后执行func函数
    }, delay);
  };
}

module.exports = {
  debounce
}