// components/df-search/df-search.js
Component({
  options: {
    styleIsolation: 'shared'
  },
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    stickyHeight: 0,
    navHeight: 46
  },

  /**
   * 组件的方法列表
   */
  methods: {
    onChange:function(e){
      console.log(e.detail);
      this.triggerEvent('onChange', e.detail)
    }
  },
  lifetimes: {
    ready: function () {

      // 获取手机系统信息
      let systemInfo = wx.getSystemInfoSync();

      // 屏幕顶部状态栏高度
      let statusBarHeight = Number(systemInfo.statusBarHeight);

      // 获取胶囊区信息
      let menu = wx.getMenuButtonBoundingClientRect();

      // 胶囊区高度
      let menuHeight = menu.height;

      // 胶囊区距离屏幕顶部的距离
      let menuTop = menu.top;

      // 屏幕顶部导航栏高度
      let navBarHeight = menu.height + (menu.top - statusBarHeight) * 2;

      // 屏幕顶部状态栏加导航栏高度
      let navStatusBarHeight = statusBarHeight + menu.height + (menu.top - statusBarHeight) * 2;

      // 屏幕底部安全区高度
      // css 可用 env(safe-area-inset-bottom) 或 constant(safe-area-inset-bottom)
      let iosSABottom = Number(systemInfo.screenHeight - systemInfo.safeArea.bottom);

      // console.log("获取手机系统信息", systemInfo);
      // console.log("获取胶囊区信息", menu);
      // console.log("胶囊区高度", menuHeight);
      // console.log("胶囊区距离屏幕顶部的距离", menuTop);
      // console.log("屏幕顶部状态栏高度", statusBarHeight);
      // console.log("屏幕顶部导航栏高度", navBarHeight);
      // console.log("屏幕顶部状态栏加导航栏高度", navStatusBarHeight);
      // console.log("屏幕底部安全区高度", iosSABottom);

      let {navHeight} = this.data
      this.setData({
        stickyHeight: statusBarHeight + navHeight
      })
    },
  }
})