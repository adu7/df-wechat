// components/df-input/index.js
Component({

  /**
   * 组件的属性列表
   */
  properties: {
    value: String,
    label: String,
    type: String,
    prompt: String,
    disabled: Boolean
  },

  /**
   * 组件的初始数据
   */
  data: {
    value: ""
  },

  /**
   * 组件的方法列表
   */
  methods: {

  }
})