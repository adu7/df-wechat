// components/df-avtear/index.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    prompt: String,
    imgdefaultUrl: Array
  },

  /**
   * 组件的初始数据
   */
  data: {
    fileList: [],
  },
  observers: {
    imgdefaultUrl: function (v)　{
      this.setData({
        fileList: [{url:"https://security.dfteco.com/"+v[0]?.url}]
      })
    }
  },
  /**
   * 组件的方法列表
   */
  methods: {
    afterRead(event) {
      const { file } = event.detail;
      if(file.fileType === "image") {
        this.setData({
          fileList: [...this.data.fileList, {url: file.tempFilePath, name: ""}]
        })
        const _this = this
        wx.uploadFile({
          url: "https://security.dfteco.com/api/v1/system/attachment/create ",
          filePath: file.url,
          name: 'file',
          header: {
            "x-token": wx.getStorageSync('token'),
            "tenant": wx.getStorageSync('tenantCode')
          },
          success(res) {
            const resdata = JSON.parse(res.data)
            _this.triggerEvent("imgUrl", {url: resdata.data.url, code: resdata.data.code})
          }
        })
      }
    },
    deleteImg(event) {
      const arr = this.data.fileList.filter((item, index) => {
        if(event.detail.index !== index) {
          return item
        }
      })
      this.setData({
        fileList: [...arr]
      })
      this.triggerEvent("deleteImgUrl", event.detail.index)
    }
  }
})