// components/df-dropdown/df-dropdown.js
Component({

  /**
   * 组件的属性列表
   */
  properties: {
    dropdownItems: null,
    selectedItem: String
  },

  /**
   * 组件的初始数据
   */
  data: {
    selectedItem:"",
    isDropdownOpen: false
  },

  /**
   * 组件的方法列表
   */
  methods: {
    toggleDropdown: function () {
      this.setData({
        isDropdownOpen: !this.data.isDropdownOpen
      });
    },
    selectItem: function (e) {
      wx.setStorageSync('tenantCode', e.target.dataset.code)
      this.setData({
        selectedItem: e.target.dataset.name,
        isDropdownOpen: false
      });
    }
  }
})