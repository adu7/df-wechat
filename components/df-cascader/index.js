// components/df-cascader/index.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    label: String,
    placeholder: String,
    value: String,
    options: Array,
    cascaderValueActive: String
  },
  observers: {
    cascaderValueActive: function(v){
      this.setData({
        cascaderValue: v
      })
    }
  },
  /**
   * 组件的初始数据
   */
  data: {
    show: false,
    fieldValue: '',
    cascaderValue: '',
    // options: [
    //   {
    //     text: '浙江省',
    //     value: '330000',
    //     children: [{ text: '杭州市', value: '330100' }],
    //   },
    //   {
    //     text: '江苏省',
    //     value: '320000',
    //     children: [{ text: '南京市', value: '320100' }],
    //   },
    // ]
  },

  /**
   * 组件的方法列表
   */
  methods: {
    onClick() {
      this.setData({
        show: true,
      });
    },
    onClose() {
      this.setData({
        show: false,
      });
    },
    onFinish(e) {
      const { selectedOptions, value } = e.detail;
      const fieldValue = selectedOptions
          .map((option) => option.text || option.name)
          .join('/');
      this.setData({
        value: fieldValue,
        cascaderValue: value,
      })
      this.triggerEvent("emitCode", this.data.cascaderValue)
    },
  }
})