import getPerson from "@/api/edit/getPerson"
import getOrganizationList from "@/api/enroll/getOrganizationList"
import getSuperList from "@/api/enroll/getSuperList"
import dataMapTrue from "@/hooks/dataMapTrue"
import findParentCodeList from "@/hooks/findParentCodeList"
import uodataPerson from "@/api/edit/updataPerson"
import Toast from '@vant/weapp/toast/toast';

// pages/edit/index.js
Page({
  /**
   * 页面的初始数据
   */
  data: {
    name: "",
    cardNumber: "",
    jobNumber: "",
    avatar:[],
    avatarUrl: [],
    faces:[],
    facesUrl:[],
    organization:"",
    organizationOptions:[],
    organizationCode: "",
    organizationAciveCode: "",
    superior:"",
    superiorOptions: [],
    superiorCode: "",
    superiorActiveCode: "",
    permissions: "",
    permissionsOptions: [
      {
        text: "普通用户",
        value: "0"
      },
      {
        text: "管理员",
        value: "1"
      }
    ],
    permissionsCode: "",
    permissionsActiveCode: ""
  },
 // 获取头像传过来的url
 getAvatarImgUrl(event) {
  this.data.avatar.push(event.detail)
  this.setData({
    avatar: [... this.data.avatar]
  })
  console.log(this.data.avatar)
},
// 获取人脸
getFacesImgUrl(event) {
  console.log(event.detail)
  this.data.faces.push(event.detail)
  this.setData({
    faces: [... this.data.faces]
  })
},
// 删除头像url
deleteAvatarImg(event) {
  console.log(event.detail)
  const arr = this.data.avatar.filter((item, index) => {
    if(event.detail !== index) {
      return item
    }
  })
  this.setData({
    avatar: [...arr]
  })
},
// 删除人脸
deleteFacesImg(event) {
  console.log(event.detail)
  const arr = this.data.faces.filter((item, index) => {
    if(event.detail !== index) {
      return item
    }
  })
  this.setData({
    faces: [...arr]
  })
},
getOrganiztionCode(e) {
  this.setData({
    organizationCode: e.detail
  })
},
getPermissionsCode(e) {
  this.setData({
    permissionsCode: e.detail
  })
},
getSuperCode(e){
  this.setData({
    superiorCode: e.detail
  })
},
onEnroll() {
  if(this.data.faces.length === 0) {
    this.setData({
      faces: this.data.facesUrl
    })
  } 
  if(this.data.avatar.length === 0) {
    this.setData({
      avatar: this.data.avatarUrl
    })
  }
  if(!this.data.organizationCode) {
    this.setData({
      organizationCode: this.data.organizationAciveCode
    })
  }
  if (!this.data.superiorCode) {
    this.setData({
      superiorCode: this.data.superiorActiveCode
    })
  }
  if(!this.data.permissionsCode) {
    this.setData({
      permissionsCode: this.data.permissionsActiveCode
    })
  }
  uodataPerson({
    avatarUrl: this.data.avatar[0]?.url,
    bioPhotoUrl: this.data.faces[0]?.url,
    code: this.data.jobNumber,
    name: this.data.name,
    icCode: this.data.cardNumber,
    organizationCode: this.data.organizationCode,
    positionCode: this.data.superiorCode,
    privilege: Number(this.data.permissionsCode)
  }).then(res =>{
    Toast.success("操作成功")
    setTimeout(()=>{
      wx.navigateTo({
        url: '/pages/person/employee/index',
      })
    },1000)
  })
},
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    getPerson({
      code: options.emplyeecode,
    }).then(res => {
      const userInfo = res.data
      console.log(res)
      const perArr = this.data.permissionsOptions.filter(item => {
        if(item.value == userInfo.privilege) {
          return item.text
        }
      })
      this.setData({
        name: userInfo.name,
        cardNumber: userInfo.icCode,
        jobNumber: userInfo.code,
        avatarUrl: [{url:userInfo.avatarUrl}],
        facesUrl: [{url:userInfo.bioPhotoUrl}],
        permissionsActiveCode: String(userInfo.privilege),
        permissions: perArr[0].text
      })
      getOrganizationList().then(tes => {
        const organizationOptions = dataMapTrue(tes.data, "")
        const arr = findParentCodeList(tes.data, userInfo.organizationCode)
        this.setData({
          organizationOptions: organizationOptions,
          organizationAciveCode: userInfo.organizationCode,
          organization: arr.join("/")
        })
      })
      getSuperList().then(yes => {
        const superOptions = dataMapTrue(yes.data, "")
        const arr = findParentCodeList(yes.data, userInfo.positionCode)
        this.setData({
          superiorOptions: superOptions,
          superiorActiveCode: userInfo.positionCode,
          superior: arr.join("/")
        })
      })
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})