import getToken from "../../api/login/getToken";
import getUserInfo from "../../api/login/getUserInfo"
// pages/start/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },
  imageOnloadError(err) {
    console.log(err)
  },
  fastLogin() {
    wx.login({
      success: ({code}) => {
        getToken({wxCode: code}).then(res => {
          wx.setStorageSync('token', res.token)
          if(wx.getStorageSync('tenantCode')) {
            wx.reLaunch({
              url: '/pages/index/index',
            })
          } else {
            wx.navigateTo({
              url: '/pages/tenant/index',
            })
          }
        }).catch(err => {
          wx.navigateTo({
            url: '/pages/login/index',
          })
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    const token = wx.getStorageSync('token')
    if(token) {
      getUserInfo().then(res => {
        if(res.code) {
        } else {
          wx.reLaunch({
            url: '/pages/index/index',
          })
        }
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})