import {usePersonEmployeeApi} from "@/api/person/employee"
Page({
  data: {
    employeeList: [],
    total:0,
    params:{
      name:'',
      pageNum:1,
      pageSize:20
    }
  },
  onLoad() {
    this.openPage()
  },
  handleChange(e) {
    let {params} = this.data
    let search = e.detail.trim()
    this.setData({
      'params.name':search
    })
    this.getEmployeeList(params,'search')
  },
  ces(){
    wx.reLaunch({
      url: '/pages/index/index',
    })
  },
  editEmployee(e){
    let {emplyeecode} = e.currentTarget.dataset
    wx.navigateTo({
      url: `/pages/edit/index?emplyeecode=${emplyeecode}`
    })
  },

  async openPage(params={pageNum:1,pageSize:20}) {
      let {data:{list,total}} =  await usePersonEmployeeApi(params)
      list = list || []
      this.setData({
        employeeList:[...list],
        total:total
      })
  },

  async getEmployeeList(params={pageNum:1,pageSize:20}) {
      let {data:{list,total}} =  await usePersonEmployeeApi(params)
      this.setData({
        employeeList:[...this.data.employeeList,...list],
        total:total
      })
  },

  async onReachBottom() {
    const {params,total,employeeList} = this.data
      if (total === employeeList.length) {
        wx.showToast({
          title: '已经加载完所有人员...',
          icon: 'success',
          duration: 1000
        })
        return
      }
    this.setData({
      'params.pageNum':params.pageNum + 1,
    })
    this.getEmployeeList(params)
  },

  addEmployee() {
    wx.navigateTo({
      url: '/pages/enroll/index'
    })
  }
})