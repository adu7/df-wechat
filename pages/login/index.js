import Toast from '@vant/weapp/toast/toast';
import bindUser from '../../api/login/baindUser' ;
// pages/login/idnex.js
Page({
  /**
   * 页面的初始数据
   */
  data: {
    user: "",
    password: "",
    isAgreement: false,
    popups: false
  },
  // 获取用户协议
  checkAgreement(e) {
    wx.setStorageSync('isAgreement', e.detail)
    this.setData({
      isAgreement: e.detail,
      popups: e.detail
    })
  },
  getIsagreement(e) {
    wx.setStorageSync('isAgreement', e.detail.result)
    this.setData({
      isAgreement: e.detail.result
    })
  },
  // 点击登录
  login() {
    // 判断用户名为空
    if(this.data.isAgreement) {
      // 判断密码为空
      if(this.data.user) {
        // 判断是否勾选用户协议
        if(this.data.password) {
          // 验证通过开始登录
          wx.login({
            success: ({code}) => {
              bindUser({
                password: this.data.password,
                username: this.data.user,
                wxCode: code
              }).then(res => {
                if(res.data !== null) {
                  wx.setStorageSync('token', res.token)
                  wx.navigateTo({
                    url: '/pages/tenant/index',
                  })
                }
              }).catch(err => {
                console.log(err.data)
                if(err.data.message === "账号密码错误") {
                }else if(err.data.message === "该微信账号已绑定，无法再次绑定") {
                  wx.navigateTo({
                    url: '/pages/start/index',
                  })
                }
              })
            },
          })
        } else {
          Toast.fail('请输入密码');
        }
      } else {
        Toast.fail('请输入用户名');
      }
    }else {
      Toast.fail('请阅读用户协议');
    }
    // wx.getUserProfile({
    //   desc: '获取用户信息',
    //   success:(res)=>{
    //     console.log(res)
    //   }
    // })
  },
  // 跳转到登录页面
  junmEnroll() {
    // wx.navigateTo({
    //   url: '/pages/enroll/index',
    // })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    const isAgreement = wx.getStorageSync("isAgreement")
    if(isAgreement) {
      this.setData({
        isAgreement: isAgreement,
        popups: !isAgreement
      })
    }else {
      this.setData({
        isAgreement: false,
        popups: !isAgreement
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})