// pages/enroll/index.js
import getOrganizationList from "@/api/enroll/getOrganizationList"
import getSuperList from "@/api/enroll/getSuperList"
import dataMapTrue from "@/hooks/dataMapTrue"
import upLoadPerson from "@/api/enroll/uploadPerson"
import Toast from '@vant/weapp/toast/toast';
Page({
  /**
   * 页面的初始数据
   */
  data: {
    name: "",
    cardNumber: "",
    jobNumber: "",
    organization:"",
    organizationCode: "",
    organizationOptions: [],
    superior:"",
    superiorOptions: [],
    superiorCode: "",
    permissions: "",
    permissionsCode: "",
    permissionsOptions: [
      {
        text: "普通用户",
        value: "1"
      },
      {
        text: "管理员",
        value: "2"
      }
    ],
    // captcha: "",
    // countDownNum: 10,
    // disabled: false,
    // text: "获取验证码",
    avatar:[],
    faces:[],
  },
  // 获取头像传过来的url
  getAvatarImgUrl(event) {
    this.data.avatar.push(event.detail)
    this.setData({
      avatar: [... this.data.avatar]
    })
    console.log(this.data.avatar)
  },
  // 获取人脸
  getFacesImgUrl(event) {
    console.log(event.detail)
    this.data.faces.push(event.detail)
    this.setData({
      faces: [... this.data.faces]
    })
  },
  // 删除头像url
  deleteAvatarImg(event) {
    console.log(event.detail)
    const arr = this.data.avatar.filter((item, index) => {
      if(event.detail !== index) {
        return item
      }
    })
    this.setData({
      avatar: [...arr]
    })
  },
  // 删除人脸
  deleteFacesImg(event) {
    console.log(event.detail)
    const arr = this.data.faces.filter((item, index) => {
      if(event.detail !== index) {
        return item
      }
    })
    this.setData({
      faces: [...arr]
    })
  },
  // 点击注册按钮
  onEnroll() {
    if(!this.data.avatar.length) {
      Toast.fail("请上传头像");
      return
    }
    if(!this.data.faces.length) {
      Toast.fail("请上传人脸");
      return
    }
    if(!this.data.jobNumber.length) {
      Toast.fail("请输入工号");
      return
    }
    if(!this.data.organizationCode.length) {
      Toast.fail("请输入选择组织");
    }
    if(!this.data.superiorCode.length) {
      Toast.fail("请选择上级");
    }
    // if(!this.data.superiorCode.length) {
    //   Toast.fail("请选择上级");
    // }
    // 新增人员接口
    upLoadPerson({
      avatarUrl: this.data.avatar[0]?.url,
      bioPhotoUrl: this.data.faces[0]?.url,
      code: this.data.jobNumber,
      name: this.data.name,
      icCode: this.data.cardNumber,
      organizationCode: this.data.organizationCode,
      positionCode: this.data.superiorCode,
      privilege: 0
    }).then(res => {
      Toast.success("新增成功")
      setTimeout(()=>{
        wx.navigateTo({
          url: '/pages/person/employee/index',
        })
      },1000)
    })
  },
  // 设置验证码倒计时
  // seTcaptcha() {
  //   let countDownNum = this.data.countDownNum; 
  //   this.setData({
  //     disabled: true
  //   })     // 获取倒计时初始值
  //   let timer = setInterval(() => {
  //     countDownNum -= 1;
  //     this.setData({
  //       countDownNum: countDownNum,
  //       text: `重新获取${countDownNum}`
  //     })
  //     if(countDownNum <= 0) {
  //       clearInterval(timer);
  //       this.setData({
  //         countDownNum: 10,
  //         disabled: false,
  //         text: `获取验证码`
  //       })
  //     }
  //   }, 1000)
  // },
  /**
   * 生命周期函数--监听页面加载
   */

  // 获取选中组织的code
  getOrganiztionCode(e) {
    this.setData({
      organizationCode: e.detail
    })
  },
  // 获取选中上级的code
  getSuperCode(e){
    this.setData({
      superiorCode: e.detail
    })
  },
  // 获取权限的code
  getPermissionsCode(e) {
    this.setData({
      permissionsCode: e.detail
    })
  },
  onLoad(options) {
    // 获取组组织列表
    getOrganizationList().then(res => {
      const options = dataMapTrue(res.data, "")
      this.setData({
        organizationOptions: options
      })
    })
    getSuperList().then(res => {
      const options = dataMapTrue(res.data, "")
      this.setData({
        superiorOptions: options
      })
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})