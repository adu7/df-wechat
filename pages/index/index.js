// pages/index/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    mainTitle: 'Hi - 欢迎来到德物智联',
    subTitle: '德行天下，服务致远',
    tenantList: [],
    tenantCode: 0,
    tenantId: "",
    tenantName: ""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    const userInfo =  wx.getStorageSync('userInfo')
    this.setData({
      tenantId: options.id
    })
    this.setData({
      tenantList: userInfo.tenantList
    })
    if(options.id) {
      this.data.tenantList.forEach(item => {
        if(item.code === options.id) {
          this.setData({
            tenantName: item.name
          })
        }
      })
    } else {
      const tenantCode = wx.getStorageSync('tenantCode')
      if(tenantCode) {
        this.data.tenantList.forEach(item => {
          if(item.code === tenantCode) {
            this.setData({
              tenantName: item.name
            })
          }
        })
      } else {
        this.setData({
          tenantName: this.data.tenantList[0].name
        })
      }
    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    if (typeof this.getTabBar === 'function' &&
      this.getTabBar()) {
      this.getTabBar().setData({
        selected: 0
      })
    }
  },

  handleAccess() {
    wx.navigateTo({
      url: '/pages/access/device/index'
      // url: '/pages/list/list?id=1&name=tom'
    })
  },

  handleEmployee() {
    wx.navigateTo({
      url: '/pages/person/employee/index'
      // url: '/pages/list/list?id=1&name=tom'
    })
  }
})