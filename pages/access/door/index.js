import {
  useAccessOpenDoorApi
} from "@/api/access/door"
import Toast from '@vant/weapp/toast/toast';

Page({
  data: {
    temperature: '26',
    weather: '阴天',
    address: '重庆',
    air: "良好",
    menu: '',
    doorCode: '',
    option: 0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.setData({
      doorCode: options.doorCode,
      option:options.option
    })
  },

  handleClick() {
    this.openDoor()
  },

  async openDoor() {
    let {
      doorCode,
      option
    } = this.data

   await useAccessOpenDoorApi({
      doorCode,
      option:Number(option)
    }) 
    Toast.success('操作成功');
  }
})