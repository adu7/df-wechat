import {useAccessDoorApi} from "@/api/access/door"

Page({
  data: {
    deviceList: [{
        deviceName: '东大门一号楼前厅',
        status: 1
      },
      {
        title: '东大门二号楼前厅',
        status: 0
      },
      {
        title: '东大门二号楼前厅',
        status: 1
      }
    ]
  },

  onLoad(options) {
    this.getDoors()
  },

  async getDoors() {
    let {data:{list}} = await useAccessDoorApi()
    this.setData({
      deviceList:list
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    console.log('2');
  },
  handleDeviceClick(e) {
    console.log(e.target.dataset.doorcode);
    let code = e.target.dataset.doorcode
    let option = e.target.dataset.option
    wx.navigateTo({
      url: `/pages/access/door/index?doorCode=${code}&option=${option}`
      // url: '/pages/access/door/index'
      // url: '/pages/list/list?id=1&name=tom'
    })
  }
})