import {useAccessInoutApi} from "@/api/access/inout"
Page({
  data: {
    inoutList: [],
    total:0,
    params:{
      employeeCode:'',
      pageNum:1,
      pageSize:20
    }
  },
  onLoad() {
    this.getInoutList()
  },
  handleChange(e) {
    let {params} = this.data
    let search = e.detail.trim()
    console.log(search,'123');
    this.setData({
      'params.employeeCode':search
    })
    this.getInoutList(params,'search')
  },
  async getInoutList(params={pageNum:1,pageSize:20},config=undefined) {
    if(config){
      console.log('1',this.data.params);
      let {data:{list,total}} =  await useAccessInoutApi(params)
      list = list || []
      this.setData({
        inoutList:[...list],
        total:total
      })
    } else {
      console.log(2);
      let {data:{list,total}} =  await useAccessInoutApi(params)
      list = list.map((item)=>{
        return {
          ...item,
          eventDate:item.eventDate.slice(0,10)
        }
      }) || []
      console.log(list,'2323');
      this.setData({
        inoutList:[...this.data.inoutList,...list],
        total:total
      })
    }
  },
  async onReachBottom() {
    const {params,total,inoutList} = this.data
      if (total === inoutList.length) {
        wx.showToast({
          title: '已经加载完所有人员...',
          icon: 'success',
          duration: 1000
        })
        return
      }
    this.setData({
      'params.pageNum':params.pageNum + 1,
    })
    this.getInoutList(params)
  }
})