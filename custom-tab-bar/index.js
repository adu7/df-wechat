Component({
  data: {
    selected: 0,
    color: "#7A7E83",
    selectedColor: "#2B75FA",
    list: [{
      pagePath: "/pages/index/index",
      iconPath: "/resource/tabbar/menu.png",
      selectedIconPath: "/resource/tabbar/menu_active.png",
      text: "菜单"
    }, {
      pagePath: "/pages/my/index",
      iconPath: "/resource/tabbar/my.png",
      selectedIconPath: "/resource/tabbar/my_active.png",
      text: "个人"
    }],
  },
  attached() {
  },
  methods: {
    switchTab(e) {
      const data = e.currentTarget.dataset
      const url = data.path
      console.log(url)
      wx.switchTab({url})
      this.setData({
        selected: data.index
      })
    }
  }
})